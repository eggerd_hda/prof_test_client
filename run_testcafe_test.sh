#!/bin/bash

if ! nc -z localhost 8080 ; then
	echo -e "\033[0;31mCan't execute testcafe tests, because prof_test_server isn't running\033[0m";
	exit 1;
fi

npm run-script start-web-server
npm run-script testcafe
npm run-script stop-web-server

