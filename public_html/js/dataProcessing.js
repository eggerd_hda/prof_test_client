/**
 * Class that processes data that is fetched through the networkAccess
 */
var dataProcessing = function () {

    /** Will be toggled if the dataProcessing is started, to prevent multiple executions */
    var running = false;

    /** Will contain all flights in the format {icao24 => {time: int, latitude: float, longitude: float} */
    var data = new Map();
    var flightsDeleted = new Map();
    var flightsUpdated = new Map();
    var flightsAdded = new Map();

    /** A list of callback functions that will be called if new data is available */
    var subscribers = [];

    /**
     * Adds a callback function to the subscriber list. The callback function
     * will be called every time new data is available, until it unsubscribes by
     * calling unsubscribe()
     * 
     * @param {function} callback
     * @return {undefined}
     */
    function subscribe(callback) {
        if (subscribers.find(function (currentValue) {
            return callback === currentValue;
        }) === undefined) {
            subscribers.push(callback);
        }
    }

    /**
     * Removes a callback function from the subscriber list, so that it doesn't
     * get called anymore, if there is new data available
     * 
     * @param {function} callback Has to be the same as passed to subscribe()
     * @return {undefined}
     */
    function unsubscribe(callback) {
        for (var i = 0; i < subscribers.length; i++) {
            if (subscribers[i] === callback) {
                subscribers.splice(i, 1);
                break;
            }
        }
    }

    /**
     * Calls all callback functions subscribed for updates with the newes 
     * available data as parameter
     * 
     * @return {undefined}
     */
    function notifySubscribers() {
        subscribers.forEach(function (sub) {
            sub({flights: data, added: flightsAdded, updated: flightsUpdated, deleted: flightsDeleted});
        });
    }

    /**
     * Initiates the polling of flight data, processes the polled data & then
     * notifies subscribers about the new data
     * 
     * @return {undefined}
     */
    function run() {
        if (running === false) {
            running = true;

            networkAccess.startPolling(function (result) {
                flightsAdded.clear();
                flightsUpdated.clear();
                flightsDeleted.clear();

                if (!result.error) {
                    var parsedResults = JSON.parse(result.data);
                    parsedResults.flights.forEach(function (flight) {
                        if (data.has(flight.icao24)) {
                            flightsUpdated.set(flight.icao24, flight);
                        } else {
                            flightsAdded.set(flight.icao24, flight);
                        }

                        data.set(flight.icao24, {time: flight.lastUpdate, latitude: flight.latitude, longitude: flight.longitude});
                    });

                    var currentTime = Math.floor(Date.now() / 1000);
                    data.forEach(function (flight, key) {
                        if (currentTime - flight.time >= 30) {
                            data.delete(key);
                            flightsDeleted.set(key, flight);
                        }
                    });

                    notifySubscribers();
                }
            });
        }
    }

    /**
     * Stops the polling, resets all flight data and unsubscribes all subscribers
     * 
     * @return {undefined}
     */
    function stop() {
        networkAccess.stopPolling();

        running = false;
        data = new Map();
        subscribers = [];
        flightsDeleted.clear();
        flightsUpdated.clear();
        flightsAdded.clear();
    }

    /**
     * Will return TRUE if polling is currently enabled, otherwise false
     * 
     * @return {Boolean}
     */
    function isRunning() {
        return running;
    }

    return {
        subscribe: subscribe,
        unsubscribe: unsubscribe,
        run: run,
        stop: stop,
        isRunning: isRunning
    };
}();

/**
 * Required for running tests with Node and using it in the browser
 */
if (typeof (exports) !== 'undefined') {
    exports.main = {
        subscribe: dataProcessing.subscribe,
        unsubscribe: dataProcessing.unsubscribe,
        run: dataProcessing.run,
        stop: dataProcessing.stop,
        isRunning: dataProcessing.isRunning
    };
}