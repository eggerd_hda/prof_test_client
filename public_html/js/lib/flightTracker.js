// provided by Prof. Renz

var flightTracker = (function () {
    // global map object
    var map;

    // the open-streetmap-tiles
    var layer_mapnik;

    // aircraft layer
    var layer_aircrafts;

    // the aircraftMap contains all current aircrafts
    var aircraftMap = {};

    // the aircraftFeaturesToDeleteMap contains all layer-element
    // that will be deleted
    var aircraftFeaturesToDelete = [];

    // fuer die Initialisierung der Karten-Darstellung
    function jumpTo(lon, lat, zoom) {
        var x = Lon2Merc(lon);
        var y = Lat2Merc(lat);
        map.setCenter(new OpenLayers.LonLat(x, y), zoom);
        return false;
    }

    // Umwandlung eines laengengrads in Map Koordinaten
    function Lon2Merc(lon) {
        return 20037508.34 * lon / 180.0;
    }

    // Umwandlung eines breitengrades in Map Koordinaten
    function Lat2Merc(lat) {
        var PI = 3.14159265358979323846;
        lat = Math.log(Math.tan((90 + lat) * PI / 360.0)) / (PI / 180.0);
        return 20037508.34 * lat / 180.0;
    }

    // when an aircraft is added, or the zoom-level is changed, the size
    // of the aircrfat is adjusted so that it does not get too big on small zoom-levels
    function getAircraftIconSize(map) {
        var zoomlevel = map.zoom;

        var newsize = 42;
        if (zoomlevel < 12 && zoomlevel > 5) {
            newsize = 42 - (12 - zoomlevel) * 2.5;
        } else if (zoomlevel <= 5) {
            newsize = 42 - (15 - zoomlevel) * 2.5;
        }

        return new OpenLayers.Size(newsize, newsize);
    }

    // called whenever a new aircraft is added to the map
    function addAircraft(layer, flight) {
        var key = flight.icao24;
        var lat = flight.latitude;
        var lon = flight.longitude;
        var altitude = flight.height;
        var heading = flight.heading;
        var callsign = flight.callsign;

        var point = new OpenLayers.Geometry.Point(lon, lat);
        point.transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913"));

        // the image that is shown
        var aircraftIconName = 'img/plane.png';
        var size = getAircraftIconSize(layer.map);
        var aircraft = new OpenLayers.Feature.Vector(point, "attributes", {
            name: key,
            externalGraphic: aircraftIconName,
            graphicWith: size.w,
            graphicHeight: size.h,
            rotation: heading

        });
//	aircraft.setId(key);

        layer.addFeatures([aircraft]);
        aircraft.flight = flight;

        return aircraft;
    }

    // React to mapEvents (we registered for zoomend)
    // this will resize the aircrafts 
    function mapEvent(event) {
        if (event.type == 'zoomend') {
            console.log("Zoomend " + map.zoom);
            var size = getAircraftIconSize(map);

            for (var key in aircraftMap) {
                if (aircraftMap[key] != null) {
                    var aircraft = aircraftMap[key].aircraft;
                    if (aircraft != null) {
                        aircraft.style.graphicWidth = size.w;
                        aircraft.style.graphicHeight = size.h;
                    }
                }
            }

            layer_aircrafts.redraw();
        }
    }

    // called for each flight, when it is added
    var addFlight = function (flight) {
        var key = flight.icao24;
        // new aircraft
        var aircraft = addAircraft(layer_aircrafts, flight);
        aircraftMap[key] = {};
        aircraftMap[key].aircraft = aircraft;
    }

    // called for each flight that is moved
    var moveFlight = function (flight) {
        var key = flight.icao24;

        if (aircraftMap[key] != null) {
            var aircraft = aircraftMap[key].aircraft;
            var newLonLat = new OpenLayers.LonLat(flight.longitude, flight.latitude)
                    .transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913"));

            aircraft.altitude = flight.height;
            aircraft.style.rotation = flight.heading;

            aircraft.move(newLonLat);
        }
    }

    var deleteFlight = function (flight) {
        var key = flight.icao24;

        if (aircraftMap[key] != null) {
            var aircraft = aircraftMap[key].aircraft;
            aircraftFeaturesToDelete.push(aircraft);

            delete aircraftMap[key];
        }
    }

    var keyArray = [];
    var addAllFlights = function (flightsToAdd) {
        // this is a "hack" to make the application testable
        // the ID of the flight images will be the same as the icao24 key
        // (we know, that this key is unique)

        // prepare ID .... so that we can test this element in the DOM ....
        keyArray = [];

        flightsToAdd.forEach(function (flight, key) {
            keyArray.push(key);
        })

        // make sure the id-generator is using the key ....
        // replace OpenLayer Dom ID generation for this layer
        var count = 0;
        var old = OpenLayers.Util.createUniqueID;
        OpenLayers.Util.createUniqueID = function (prefix) {
            if (prefix.search('^OpenLayers.Geometry.Point_') >= 0) {
                // special Dom IDs for Geometry nodes only; these are the aircrafs beeing added
                return keyArray[count++];
            }
            // default to using the previous ID generator...
            return(old(prefix));
        }

        flightsToAdd.forEach(addFlight);
        OpenLayers.Util.createUniqueID = old;
    }

    // These are the public functions of this module:
    // this function is called with the data received from the prof_test_server
    // it handles adding, moving and deleting flight-objects in the map
    var flightDataProcessor = function (data) {
        if (data.errorStatus) {
            // display some error message ....
            // delete unused flight objects
            return;
        }

        aircraftFeaturesToDelete = [];
        data.deleted.forEach(deleteFlight);
        layer_aircrafts.destroyFeatures(aircraftFeaturesToDelete);

        addAllFlights(data.added);
        data.updated.forEach(moveFlight);

        layer_aircrafts.redraw();
    }

    // Main routine, draws the initial map
    function drawmap() {
        OpenLayers.Lang.setCode('de');

        // Position und Zoomstufe der Karte
        var lon = 6.7;
        var lat = 49.8;
        var zoom = 8;

        map = new OpenLayers.Map('map', {
            projection: new OpenLayers.Projection("EPSG:900913"),
            displayProjection: new OpenLayers.Projection("EPSG:4326"),
            controls: [
                new OpenLayers.Control.Navigation(),
                // new OpenLayers.Control.LayerSwitcher(),
                new OpenLayers.Control.PanZoomBar()],
            eventListeners: {
                "zoomend": mapEvent
            },
            maxExtent:
                    new OpenLayers.Bounds(-20037508.34, -20037508.34,
                            20037508.34, 20037508.34),
            numZoomLevels: 18,
            maxResolution: 156543,
            units: 'meters'
        });

        // this puts the open-streetmap-tiles in a layer
        layer_mapnik = new OpenLayers.Layer.OSM.Mapnik("Mapnik");
        layer_mapnik.setOpacity(0.5);

        // this will be for the aircrafts 
        layer_aircrafts = new OpenLayers.Layer.Vector("aircrafts", {
            style: OpenLayers.Feature.Vector.style["default"]});

        map.addLayers([layer_mapnik, layer_aircrafts]);
        jumpTo(lon, lat, zoom);
    }

    function getVector(key) {
        return aircraftMap[key].aircraft;
    }

    return {
        drawmap: drawmap,
        flightDataProcessor: flightDataProcessor,
        getVector: getVector
    }
})();