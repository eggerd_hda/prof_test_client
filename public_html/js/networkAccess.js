/**
 * Class that handles all network related tasks, like fetching the data
 */
var networkAccess = function () {

    // These properties are made public and can be overwritten
    var properties = {
        /** URL to the server that returns the flight data as JSON string */
        url: 'http://127.0.0.1:8080/flights',

        /** Time in milliseconds between polls */
        pollTime: 10000
    }

    /** Will contain a reference to the poll timeout */
    var poller = null;

    /**
     * Fetchs the data from the server
     * 
     * @param {function} callback
     * @return {error: boolean, data: string}
     */
    function getData(callback) {
        var httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function (readystate) {
            if (httpRequest.readyState === XMLHttpRequest.DONE) {
                if (httpRequest.status === 200) {
                    callback({error: false, data: httpRequest.responseText});
                } else {
                    console.warn('Failed to fetch data! Status: ' + httpRequest.status);
                    callback({error: true});
                }
            }
        };

        httpRequest.open("GET", properties.url);
        httpRequest.send();
    }

    /**
     * Initiates the polling of flight data
     * 
     * @param {function} callback
     * @return {undefined}
     */
    function startPolling(callback) {
        if (poller === null) {
            getData(function (response) {
                callback(response);
                pollData(callback);
            });
        }
    }

    /**
     * This function actually performs the polling. It will call itself after
     * waiting for the time specified in pollTime, until it is stopped by calling
     * stopPolling()
     * 
     * @param {function} callback
     * @return {undefined}
     */
    function pollData(callback) {
        poller = setTimeout(function () {
            getData(function (response) {
                callback(response);
                pollData(callback);
            });
        }, properties.pollTime);
    }

    /**
     * Stops the polling of flight data by resetting the timeout
     * 
     * @return {undefined}
     */
    function stopPolling() {
        clearTimeout(poller);
        poller = null;
    }

    return {
        getData: getData,
        startPolling: startPolling,
        stopPolling: stopPolling,
        properties: properties
    };
}();

/**
 * Required for running tests with Node and using it in the browser
 */
if (typeof (exports) !== 'undefined') {
    exports.main = {
        getData: networkAccess.getData,
        startPolling: networkAccess.startPolling,
        stopPolling: networkAccess.stopPolling,
        properties: networkAccess.properties
    };
}