if (typeof module !== 'undefined' && module.exports) {
    // this is done, if we are running with NODE
    networkAccess = require('../js/networkAccess.js').main;
    dataProcessing = require('../js/dataProcessing.js').main;

    var assert = require('chai').assert;
    var sinon = require('sinon');
} else {
    // this is done, if we are in the browser
    var expect = chai.expect;
    var assert = chai.assert;
}

var xhrRequest;
before(function () {
    XMLHttpRequest = sinon.useFakeXMLHttpRequest();
    XMLHttpRequest.onCreate = function (xhr) {
        xhrRequest = xhr;
    };
});

beforeEach(function () {
    dataProcessing.stop(); // stops the polling and resets the data
});

describe('subscribe', function () {
    /**
     * TODO: How to test a function like this? The function that would trigger 
     * the callback of this function (aka notifySubcribers) is private, so we 
     * can't test the response. Moreover, all properties this function is using
     * are private as well, so we can't even trigger the response manually...
     * 
     * Making everything public can't be the answer?
     * 
     * Is currently tested implcit during the processing test
     */
});

describe('run', function () {
    it('should prevent multiple executions', function () {
        var stub = sinon.stub(networkAccess, 'startPolling').callsFake(function (callback) {
            callback({error: true});
        });

        dataProcessing.run();
        dataProcessing.run();
        assert.equal(1, stub.callCount);

        stub.restore();
    });

    /*it('should handle unexpected data', function () {
     var stubCallback;
     var stubCallCount = 0;
     var stubFunction = function (callback) {
     stubCallback = callback;
     stubCallCount++;
     
     stubCallCount === 1 ? callback() : null;
     stubCallCount === 2 ? callback({}) : null;
     stubCallCount === 3 ? callback({error: false, data: ""}) : null;
     stubCallCount === 4 ? callback({error: false, data: null}) : null;            
     stubCallCount === 5 ? callback({error: false, data: null}) : null;
     }
     var stub = sinon.stub(networkAccess, 'startPolling').callsFake(stubCallback);
     
     // TODO: How to validate? Subscibers should not be called & there is no
     // direct access to the data itself - making it accessable just for tests?
     
     dataProcessing.run();
     });*/

    /**
     * - it should add new flights to the list & update existing flights
     * - it should remove flights with a inactivity of >=30 seconds
     * - it should notify subscribers after the data has been processed
     */
    it('should process the data', function (done) {
        var currentTime = Math.floor(Date.now() / 1000);
        var response1 = {"time": currentTime - 25, "flights": [
                {"latitude": 31.3525, "longitude": -96.0197, "icao24": "ab1644", "lastUpdate": currentTime - 28}, // should be deleted after the third poll
                {"latitude": -2.1342, "longitude": 104.1495, "icao24": "bd1975", "lastUpdate": currentTime - 27}]}; // should not be deleted, because it got updated during the second poll
        var response2 = {"time": currentTime - 10, "flights": [{"latitude": -5.4213, "longitude": 105.9514, "icao24": "bd1975", "lastUpdate": currentTime - 10}]};
        var response3 = {"time": currentTime, "flights": [{"latitude": -44.4213, "longitude": 55.1981, "icao24": "ca7361", "lastUpdate": currentTime}]};

        var stubCallback;
        var stubCallCount = 0;
        var stubFunction = function (callback) {
            stubCallback = callback;
            stubCallCount++;

            stubCallCount === 1 ? callback({error: false, data: JSON.stringify(response1)}) : null;
            stubCallCount === 2 ? callback({error: false, data: JSON.stringify(response2)}) : null;
            stubCallCount === 3 ? callback({error: false, data: JSON.stringify(response3)}) : null;
        }
        var stub = sinon.stub(networkAccess, 'startPolling').callsFake(stubFunction);

        var subscriberCallback2 = function () {
            if (stubCallCount > 2) {
                stub.restore();
                assert.fail(stubCallCount, 2, "Incorrect amount of calls to the subscriber callback 2");
            }
        }

        dataProcessing.subscribe(subscriberCallback2);
        dataProcessing.subscribe(subscriberCallback2); // should not be added twice to the list of subscribers
        dataProcessing.subscribe(function (response) {
            if (stubCallCount === 1) {
                assert.equal(2, response.flights.size);

                // validating the first flight
                var f1 = response.flights.get('ab1644');
                var r1 = response1.flights[0];
                assert.equal(r1.latitude, f1.latitude);
                assert.equal(r1.longitude, f1.longitude);
                assert.equal(r1.lastUpdate, f1.time);

                // validating the second flight
                var f2 = response.flights.get('bd1975');
                var r2 = response1.flights[1];
                assert.equal(r2.latitude, f2.latitude);
                assert.equal(r2.longitude, f2.longitude);
                assert.equal(r2.lastUpdate, f2.time);

                // simulates the next poll
                setTimeout(function () {
                    stubFunction(stubCallback);
                }, 1000);
            } else if (stubCallCount === 2) {
                assert.equal(2, response.flights.size);

                // validating the first flight hasn't been deleted, although it has not been updated
                var f1 = response.flights.get('ab1644');
                var r1 = response1.flights[0];
                assert.equal(r1.latitude, f1.latitude);
                assert.equal(r1.longitude, f1.longitude);
                assert.equal(r1.lastUpdate, f1.time);

                // validating the second flight has been updated
                var f2 = response.flights.get('bd1975');
                var r2 = response2.flights[0];
                assert.equal(r2.latitude, f2.latitude);
                assert.equal(r2.longitude, f2.longitude);
                assert.equal(r2.lastUpdate, f2.time);

                dataProcessing.unsubscribe(subscriberCallback2);

                // simulates the next poll
                setTimeout(function () {
                    stubFunction(stubCallback);
                }, 1000);
            } else if (stubCallCount === 3) {
                assert.equal(2, response.flights.size);

                // validating the first flight has been deleted, due to inactivity
                assert(!response.flights.has('ab1644'));

                // validating the first second hasn't been deleted, although it has not been updated
                var f1 = response.flights.get('bd1975');
                var r1 = response2.flights[0];
                assert.equal(r1.latitude, f1.latitude);
                assert.equal(r1.longitude, f1.longitude);
                assert.equal(r1.lastUpdate, f1.time);

                // validating the thrid flight has been added
                var f2 = response.flights.get('ca7361');
                var r2 = response3.flights[0];
                assert.equal(r2.latitude, f2.latitude);
                assert.equal(r2.longitude, f2.longitude);
                assert.equal(r2.lastUpdate, f2.time);

                dataProcessing.unsubscribe(subscriberCallback2); // is already unsubscribed; shouldn't cause any problems

                stub.restore();
                done();
            } else {
                stub.restore();
                assert.fail(stubCallCount, 3, "Incorrect amount of calls to the subscriber callback");
            }
        });

        dataProcessing.run();
    }).timeout(3000);

    it("shouldn't notify subscribers or reset data, if an error occurs during polling", function () {
        var currentTime = Math.floor(Date.now() / 1000);
        var response1 = {"time": currentTime, "flights": [{"latitude": 31.3525, "longitude": -96.0197, "icao24": "ab1644", "lastUpdate": currentTime}]};
        var response2 = {"time": currentTime, "flights": [{"latitude": -5.4213, "longitude": 105.9514, "icao24": "bd1975", "lastUpdate": currentTime}]};

        var stubCallback;
        var stubCallCount = 0;
        var stubFunction = function (callback) {
            stubCallback = callback;
            stubCallCount++;

            stubCallCount === 1 ? callback({error: false, data: JSON.stringify(response1)}) : null;
            stubCallCount === 3 ? callback({error: false, data: JSON.stringify(response2)}) : null;

            if (stubCallCount === 2) {
                callback({error: true});
                subscriberCallback('not called through notifySubscribers');
            }
        }
        var stub = sinon.stub(networkAccess, 'startPolling').callsFake(stubFunction);

        var subscriberCallback = function (response) {
            if (stubCallCount === 1) {
                assert.equal(1, response.flights.size);

                // validating the flight has been added
                var f1 = response.flights.get('ab1644');
                var r1 = response1.flights[0];
                assert.equal(r1.latitude, f1.latitude);
                assert.equal(r1.longitude, f1.longitude);
                assert.equal(r1.lastUpdate, f1.time);

                stubFunction(stubCallback);
            } else if (stubCallCount === 2) {
                /**
                 * The subscriberCallback should not be called by run(), because
                 * the there was an error during polling. But in order to proceed
                 * with the test the callback is manually called by our stub
                 */
                assert.equal('not called through notifySubscribers', response);
                stubFunction(stubCallback);
            } else if (stubCallCount === 3) {
                assert.equal(2, response.flights.size);

                // validating the flight from the first poll hasn't been deleted due to the error
                var f1 = response.flights.get('ab1644');
                var r1 = response1.flights[0];
                assert.equal(r1.latitude, f1.latitude);
                assert.equal(r1.longitude, f1.longitude);
                assert.equal(r1.lastUpdate, f1.time);

                stub.restore();
            } else {
                stub.restore();
                assert.fail(stubCallCount, 3, "Incorrect amount of calls to the subscriber callback");
            }
        }

        dataProcessing.subscribe(subscriberCallback);
        dataProcessing.run();
    });
});

describe('isRunning', function () {
    it('should return the correct status', function () {
        assert.equal(false, dataProcessing.isRunning());

        dataProcessing.run();
        assert.equal(true, dataProcessing.isRunning());
    });
});