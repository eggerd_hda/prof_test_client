/**
 * Created by kaire on 08.06.2018.
 */

var isBrowser = false;
if (typeof module !== 'undefined' && module.exports) {
    // this is done, if we are running with NODE
    var flightTracker = require('../js/lib/flightTracker.js').main;

    var assert = require('chai').assert;
    var sinon = require('sinon');

} else {
    // this is done, if we are in the browser
    var expect = chai.expect;
    var assert = chai.assert;
    isBrowser = true;
}

if (isBrowser) {
    describe('Map handling', function () {
        it('should show a map', function () {
            flightTracker.drawmap();
            // wait for the map to show?
        });

        it('should draw a plane', function () {
            var flightOne = {icao24: "ABC123", longitude: 8, latitude: 50, height: 100, heading: 90, callsign: "DLH123"};
            var flightsMap = new Map();
            var addedMap = new Map();
            var updatedMap = new Map();
            var deletedMap = new Map();

            flightsMap.set(flightOne.icao24, flightOne);
            addedMap.set(flightOne.icao24, flightOne);

            flightTracker.flightDataProcessor(
                    {errorStatus: false, errorMessage: null, lastUpdate: 1234, flights: flightsMap, added: addedMap, updated: updatedMap, deleted: deletedMap}
            );

            // now we make sure that the ABC123 image is visible in the DOM ...
            var myElement = document.getElementById(flightOne.icao24);
            assert(myElement);
            assert.equal(flightOne.heading, myElement._style.rotation);

            // we get the Feature-Object from OpenLayers
            var aircraft = flightTracker.getVector(flightOne.icao24);
            var checkLocation = new OpenLayers.LonLat(flightOne.longitude, flightOne.latitude)
                    .transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913"));
            assert(aircraft.atPoint(checkLocation, 1, 1));
        });

        it('should draw a plane other rotation', function () {
            var flightOne = {icao24: "ABA1", longitude: 7, latitude: 50, height: 200, heading: 270, callsign: "DLH678"};
            var flightsMap = new Map();
            var addedMap = new Map();
            var updatedMap = new Map();
            var deletedMap = new Map();

            flightsMap.set(flightOne.icao24, flightOne);
            addedMap.set(flightOne.icao24, flightOne);

            flightTracker.flightDataProcessor(
                    {errorStatus: false, errorMessage: null, lastUpdate: 1234, flights: flightsMap, added: addedMap, updated: updatedMap, deleted: deletedMap}
            );

            // now we make sure that the ABA1 image is visible in the DOM ...
            var myElement = document.getElementById(flightOne.icao24);
            assert(myElement);
            assert.equal(flightOne.heading, myElement._style.rotation);

            // we get the Feature-Object from OpenLayers
            var aircraft = flightTracker.getVector(flightOne.icao24);
            var checkLocation = new OpenLayers.LonLat(flightOne.longitude, flightOne.latitude)
                    .transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913"));
            assert(aircraft.atPoint(checkLocation, 1, 1));
        });

        it('should update a plane', function () {
            var flightOne = {icao24: "ABC123", longitude: 8.2, latitude: 50.2, height: 100, heading: 100, callsign: "DLH123"};
            var flightsMap = new Map();
            var addedMap = new Map();
            var updatedMap = new Map();
            var deletedMap = new Map();

            flightsMap.set(flightOne.icao24, flightOne);
            updatedMap.set(flightOne.icao24, flightOne);

            flightTracker.flightDataProcessor(
                    {errorStatus: false, errorMessage: null, lastUpdate: 1234, flights: flightsMap, added: addedMap, updated: updatedMap, deleted: deletedMap}
            );

            // now we make sure that the ABC123 image is visible in the DOM ...
            var myElement = document.getElementById(flightOne.icao24);
            assert(myElement);
            assert.equal(flightOne.heading, myElement._style.rotation);

            // we get the Feature-Object from OpenLayers
            var aircraft = flightTracker.getVector(flightOne.icao24);
            var checkLocation = new OpenLayers.LonLat(flightOne.longitude, flightOne.latitude)
                    .transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913"));
            assert(aircraft.atPoint(checkLocation, 1, 1));
        });

        // we need a test for the correct removal ....
    });
}