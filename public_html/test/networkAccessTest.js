if (typeof module !== 'undefined' && module.exports) {
    // this is done, if we are running with NODE
    networkAccess = require('../js/networkAccess.js').main;

    var assert = require('chai').assert;
    var sinon = require('sinon');
} else {
    // this is done, if we are in the browser
    var expect = chai.expect;
    var assert = chai.assert;
}

var xhrRequest;
before(function () {
    XMLHttpRequest = sinon.useFakeXMLHttpRequest();
    XMLHttpRequest.onCreate = function (xhr) {
        xhrRequest = xhr;
    };

    networkAccess.properties.pollTime = 50;
});

describe('getData', function () {
    it('should handle unsuccessfull requests and responde accordingly', function (done) {
        networkAccess.getData(function (result) {
            assert.equal(true, result.error);
            assert.equal(undefined, result.data);
            done();
        });

        xhrRequest.respond(500);
    });

    it('should return the response of the request, if it was successfull', function (done) {
        networkAccess.getData(function (result) {
            assert.equal(false, result.error);
            assert.equal('Hello World!', result.data);
            done();
        });

        xhrRequest.respond(200, {"content-type": "text/plain"}, 'Hello World!');
    });
});

describe('startPolling', function () {
    it("shouldn't allow to start multiple polling loops", function () {
        var callbackCalled = 0;
        networkAccess.startPolling(function () {
            // this callback should be called by startPolling
            callbackCalled++;
        });

        xhrRequest.respond(200);

        networkAccess.startPolling(function () {
            // this callback should NOT be called by startPolling
            fail('seconds poll loop started!');
        });

        /**
         * Validating that the old xhrRequest from the first startPolling() call
         * hasn't been replaced by the second call
         */
        assert.equal(200, xhrRequest.status);
        assert.equal(1, callbackCalled);

        networkAccess.stopPolling();
    });

    it("should call getData and pass the response to the callback", function (done) {
        var callbackCalled = 0;
        networkAccess.startPolling(function (response) {
            assert(!response.error);
            assert.equal('Hello World!', response.data);
            callbackCalled++;
        });

        xhrRequest.respond(200, {"content-type": "text/plain"}, 'Hello World!');

        // validating that the callback is called again, after the next poll
        setTimeout(function () {
            xhrRequest.respond(200, {"content-type": "text/plain"}, 'Hello World!');
            assert.equal(2, callbackCalled);
            networkAccess.stopPolling();
            done();
        }, networkAccess.properties.pollTime);
    }).timeout(networkAccess.properties.pollTime + 50);
});

describe('stopPolling', function () {
    it("shouldn't care, if polling hasn't even started", function () {
        networkAccess.stopPolling();
        assert(true); // should be reached, as long as stopPolling doesn't throw an exception
    });

    it("should interupt the polling loop", function (done) {
        var callbackCalled = 0;
        networkAccess.startPolling(function () {
            callbackCalled++;
        });

        xhrRequest.respond(200);
        networkAccess.stopPolling();

        // validating that the callback isn't called again
        setTimeout(function () {
            /**
             * Validating that the old xhrRequest from the first startPolling() 
             * call hasn't been replaced by a second poll
             */
            assert.equal(200, xhrRequest.status);
            assert.equal(1, callbackCalled);
            done();
        }, networkAccess.properties.pollTime);
    }).timeout(networkAccess.properties.pollTime + 50);

    it("shouldn't prevent a restart of the polling", function () {
        var callbackCalled = 0;
        networkAccess.startPolling(function () {
            callbackCalled++;
        });

        xhrRequest.respond(200);
        networkAccess.stopPolling();

        networkAccess.startPolling(function () {
            callbackCalled++;
        });

        xhrRequest.respond(200);
        networkAccess.stopPolling();

        assert.equal(2, callbackCalled);
    });
});