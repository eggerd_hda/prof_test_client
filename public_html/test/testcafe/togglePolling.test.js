import { Selector } from 'testcafe';
import { main as networkAccess } from '../../js/networkAccess';

networkAccess.properties.pollTime = 1000;
fixture `prof_test_client`.page `http://localhost:7774/`;
const status = Selector('#status');
const button = Selector('#toggle');

test('Polling should be enabled upon opening the page', async t => {
    await t.expect(status.innerText).eql('polling');
    await t.expect(button.innerText).eql('Stop');
});

test('Status should be updated correctly when toggling polling', async t => {
    await t.expect(status.innerText).eql('polling');
    await t.expect(button.innerText).eql('Stop');

    await t
            .click(button)
            .expect(status.innerText).eql("paused")
            .expect(button.innerText).eql("Start");

    await t
            .click(button)
            .expect(status.innerText).eql("polling")
            .expect(button.innerText).eql("Stop");
});

test('Stats should only change if polling is enabled', async t => {
    const statSum = Selector('#amountFlights');
    const statAdded = Selector('#amountFlightsAdded');
    const statUpdated = Selector('#amountFlightsUpdated');
    const statDeleted = Selector('#amountFlightsDeleted');

    const statSumValue = await statSum.innerText;
    const statAddedValue = await statAdded.innerText;
    const statUpdatedValue = await statUpdated.innerText;
    const statDeletedValue = await statDeleted.innerText;

    await t
            .click(button)
            .wait(networkAccess.properties.pollTime * 2)
            .expect(statSum.innerText).eql(statSumValue)
            .expect(statAdded.innerText).eql(statAddedValue)
            .expect(statUpdated.innerText).eql(statUpdatedValue)
            .expect(statDeleted.innerText).eql(statDeletedValue);

    await t
            .click(button)
            .wait(networkAccess.properties.pollTime * 2)
            .expect(statSum.innerText).gt(0)
            .expect(statAdded.innerText).gt(0)
            .expect(statUpdated.innerText).eql("0")
            .expect(statDeleted.innerText).gte(0);
});