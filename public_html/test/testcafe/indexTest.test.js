import { Selector } from 'testcafe';
fixture `prof_test_client`.page `http://localhost:7774/indexTest.html`;

test('All tests should succeed', async t => {
    const status = Selector('.test.fail');
    await t.expect(status.exists).notOk();
});