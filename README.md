# Professionelles Testen - Client
*5\. Semester, 2018 (Wahlpflicht)*  
*Prof. Dr. Kai Renz*

Hierbei handelt es sich um die zweite Hälfte des Praktikum, in der von uns ein Web-Client programmiert und getestet wurde, der die vom Server bereitgestellten Flugpositionsdaten auf einer Landkarte anzeigt.

Der zugehörige Server wurde von uns in der [ersten Hälfte des Praktikum](https://gitlab.com/eggerd_hda/prof_test_server) programmiert.


## Behandelte Themen
- Testen im JavaScript Umfeld mit [SinonJS](https://sinonjs.org/)
- Testen von asynchronen Funktionen mit [Mocha](https://mochajs.org/)
- Code-Coverage mit [Istanbul](https://istanbul.js.org/)
- Oberflächentests im Browser mit [Testcafé](https://github.com/DevExpress/testcafe)
- Whitebox-Tests
- Mocks und Stubs
- Observer-Pattern


## Anforderungen
- Auf einer Landkarte soll die jeweils aktuelle Position und Flugrichtung von Flugzeugen zu sehen sein
- Die Flugpositionsdaten werden hierzu regelmäßig vom Server abgerufen werden
- Die Abfrage der Flugpositionsdaten soll vom Benutzer über einen Button pausiert werden können
- Der aktuell sichtbare Bereich der Landkarte soll automatisch als geographischer Filter auf dem Server gesetzt werden, so dass nur Flugzeuge übermittelt werden die auch aktuell für den Benutzer sichtbar sind
- Flugzeuge die länger als 30 Sekunden kein Positionsupdate erhalten haben sollen automatisch von der Landkarte gelöscht werden
- Beim Klick auf ein Flugzeugsymbol soll sich ein Dialogfenster öffnen, in dem die Kennung, die Höhe und die Geschwindigkeit angezeigt wird
- Selbst implementierte Funktionen, sowie die Funktionalität der Benutzeroberfläche im Browser, sollten möglichst vollständig (Zweigüberdeckung) getestet sein, mit Hilfe von...
    - Unit- & Oberflächentests
    - Mocks/Stubs
    - etc.


## Endergebnis
[![Demo GIF](https://gitlab.com/eggerd_hda/prof_test_client/-/raw/assets/demo.gif)](https://gitlab.com/eggerd_hda/prof_test_client/-/raw/assets/demo.mp4)
- Jeglicher von uns implementierte Code ist nahezu vollständig zweigüberdeckend getestet ![Code Coverage](https://gitlab.com/eggerd_hda/prof_test_client/-/raw/assets/coverage.png)
- Zudem das folgende Verhalten der Benutzeroberfläche getestet:
    - Werden die Abfragen pausiert, soll dies entsprechend visualisiert werden (und umgekehrt)
    - Direkt nach dem Öffnen der Webseite sollte die kontinuierliche Abfrage der Flugpositionsdaten starten
    - Die angezeigten Statistiken sollten sich nur verändern, wenn die Abfragen zurzeit aktiviert sind
- Aus Zeitmangel wurden einige Anforderungen vom Professor verworfen:
    - Beim Klick auf ein Flugzeugsymbol öffnet sich kein Dialogfenster mit näheren Details zum Flugzeug
    - Der aktuell sichtbare Bereich der Landkarte wird nicht automatisch als geographischer Filter auf dem Server gesetzt, auch wenn wir hierzu extra eine entsprechende [REST-Schnittstelle implementiert hatten](https://gitlab.com/eggerd_hda/prof_test_server#rest-schnittstelle)



## Demo

##### Docker
Der einfachste Weg ist die Verwendung von [Docker](https://www.docker.com/), da der bereitgestellte [Container](https://hub.docker.com/r/eggerd/hda-prof-test) bereits alle Abhängigkeiten mit sich bring und sowohl den Web-Client als auch den [Server](https://gitlab.com/eggerd_hda/prof_test_server) bereitstellt.

Über den nachfolgenden Befehl kann der Container heruntergeladen und gestartet werden.
```sh
docker run -it -p 8080:8080 -p 8383:8383 eggerd/hda-prof-test
```
Sobald im Terminal die Nachricht `Starting up http-server` erscheint, ist der Startvorgang abgeschlossen und der Web-Client ist nun über einen beliebigen Browser unter `http://localhost:8383` zu erreichen.


##### Manuell
Alternativ kann der Web-Client auch einfach heruntergeladen und anschließend die Datei `/public_html/index.html` in einem beliebigen Browser geöffnet werden. Allerdings muss zusätzlich der [Server installiert werden](https://gitlab.com/eggerd_hda/prof_test_server#installation), damit auch Flugzeuge auf der Landkarte angezeigt werden können.


## Beteiligte Personen
- [Dustin Eckhardt](https://gitlab.com/eggerd)
- Johannes Reichard


## Verwendete Software
- [SinonJS](https://sinonjs.org/) (5.0.7)
- [Mocha](https://mochajs.org/) (5.0.4)
- [Node.js](https://nodejs.org/) (8.11.1)
- [Istanbul](https://istanbul.js.org/) (0.4.5)
- [nyc](https://github.com/istanbuljs/nyc) (11.7.1)
- [Chai](https://www.chaijs.com/) (4.1.2)
- [Testcafé](https://github.com/DevExpress/testcafe) (0.20.2)
- [Jest](https://jestjs.io/) (23.0.0)
- [http-server](https://github.com/indexzero/http-server) (0.11.1)
- [OpenLayers](https://openlayers.org/) (2.13.1)
- [OpenStreetMap](https://www.openstreetmap.de/)